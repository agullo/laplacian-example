module grid_n_operators
contains

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine Laplacian(f_in,f_out)
use parameters
implicit none
real*8,     dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext), intent(out) :: f_out
real*8,     dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext), intent(in)  :: f_in  

! it calculated the laplacien of the field according the non symetric algorythm.

! uni_grid True, coef are simpler :
! mr2l = mr2r = 1/dr**2
! nr2l = nr2r = 1/dr**2
! sr2l = sr2r = -2/dr**2

! mz2l = mz2r = 1/dz**2
! nz2l = nz2r = 1/dz**2
! sz2l = sz2r = -2/dz**2

! p*** = 0.

! mr1l = mr1r = -1/(2*dr)
! nr1l = nr1l = 1/(2*dr)
! sr1l = sr1r = 0.

! mz1l = mz1r = -1/(2*dz)
! nz1l = nz1l = 1/(2*dz)
! sz1l = sz1r = 0.


do iz = Sz,Ez_p
do ir = Sr,Er_p 

!left algo r and z
f_out(ir,iz) =   (mr2l(ir) - mr1(ir)*ri(ir))*f_in(ir-1,iz)&
               + (nr2l(ir) - nr1(ir)*ri(ir))*f_in(ir+1,iz)&
               +             pr2l(ir)*f_in(ir+2,iz)&
               +             mz2l(iz)*f_in(ir,iz-1)&
               +             nz2l(iz)*f_in(ir,iz+1)&
               +             pz2l(iz)*f_in(ir,iz+2)&
               + (sz2l(iz) + sr2l(ir) - sr1(ir)*ri(ir))*f_in(ir,iz)              
enddo
enddo

!right algo r, left z
if( .not.(para == 'R' .and. MYID < NUMPROCS-1))then
   do iz= Sz,Ez_p
f_out(NR,iz) =   (mr2r - mr1(NR)*ri(NR))*f_in(NR-1,iz)&
               + (nr2r - nr1(NR)*ri(NR))*f_in(NR+1,iz)&
               +             pr2r*f_in(NR-2,iz)&
               +             mz2l(iz)*f_in(NR,iz-1)&
               +             nz2l(iz)*f_in(NR,iz+1)&
               +             pz2l(iz)*f_in(NR,iz+2)&
               + (sz2l(iz) + sr2r - sr1(NR)*ri(NR))*f_in(NR,iz)
    enddo
endif

if( .not.(para == 'Z' .and. MYID < NUMPROCS-1))then
   do ir= Sr,Er_p
!left algo r, right z
f_out(ir,Nz) =   (mr2l(ir) - mr1(ir)*ri(ir))*f_in(ir-1,Nz)&
               + (nr2l(ir) - nr1(ir)*ri(ir))*f_in(ir+1,Nz)&
               +             pr2l(ir)*f_in(ir+2,Nz)&
               +             mz2r*f_in(ir,Nz-1)&
               +             nz2r*f_in(ir,Nz+1)&
               +             pz2r*f_in(ir,Nz-2)&
               + (sz2r + sr2l(ir) - sr1(ir)*ri(ir))*f_in(ir,Nz)  
       enddo
endif

if(MYID == NUMPROCS - 1)then
!right algo r, right z
f_out(NR,NZ) =   (mr2r - mr1(NR)*ri(NR))*f_in(NR-1,Nz)&
               + (nr2r - nr1(NR)*ri(NR))*f_in(NR+1,Nz)&
               +             pr2r*f_in(NR-2,Nz)&
               +             mz2r*f_in(NR,Nz-1)&
               +             nz2r*f_in(NR,Nz+1)&
               +             pz2r*f_in(NR,Nz-2)&
               + (sz2r + sr2r - sr1(NR)*ri(NR))*f_in(NR,Nz) 
endif


end subroutine Laplacian
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine Rgradient(f_in,f_out)
use parameters
implicit none
real*8,     dimension(Sr:Er_ext,Sz:Ez_ext), intent(out) :: f_out
real*8,     dimension(Sr:Er_ext,Sz:Ez_ext), intent(in)  :: f_in  

! it calculate radial gradient (non homogeneous algorythm is symetric for this operator)
do iz = Sz,Ez
do ir = Sr,Er
f_out(ir,iz) =  mr1(ir)*f_in(ir-1,iz) + sr1(ir)*f_in(ir,iz) +nr1(ir)*f_in(ir+1,iz)
enddo
enddo

end subroutine Rgradient
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine Zgradient(f_in,f_out)
use parameters
implicit none
real*8,     dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext), intent(out) :: f_out
real*8,     dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext), intent(in)  :: f_in  

! it calculate radial gradient (non homogeneous algorythm is symetric for this operator)
do iz = Sz,Ez
do ir = Sr,Er
f_out(ir,iz) =  mz1(iz)*f_in(ir,iz-1) + sz1(iz)*f_in(ir,iz) +nz1(iz)*f_in(ir,iz+1)
enddo
enddo

end subroutine Zgradient
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine CB_box_w
use parameters
implicit none

! boundary condition are set only for boundary processors with myid = 0 for bot boundary and with myid = NUMPROCS-1 for top boundary.
if( (para == 'Z' .and. MYID == NUMPROCS-1) .or. (para == 'R') )then
  w(Sr:Er,Nz+1) =  0.
endif

if((para == 'Z' .and. MYID == 0) .or. (para == 'R') )then
    w(Sr:Er,0) =  0.
endif

! boundary condition are set for all processors for righ and left boundaries.
if( (para == 'R' .and. MYID == NUMPROCS-1) .or. (para == 'Z') )then
  w(NR+1,Sz:Ez) = 0.
endif  

if((para == 'R' .and. MYID == 0) .or. (para == 'Z') )then
     w(0,Sz:Ez) = 0.
endif

end subroutine CB_box_w
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine CB_box_lapw
use parameters
implicit none


! boundary condition are set only for boundary processors with myid = 0 for bot boundary and with myid = NUMPROCS-1 for top boundary.
if( (para == 'Z' .and. MYID == NUMPROCS-1) .or. (para == 'R') )then
  lapw(Sr:Er,Nz+1) =  2.*w(Sr:Er,Nz)/(z(NZ+1)-z(NZ))**2
endif

if((para == 'Z' .and. MYID == 0) .or. (para == 'R') )then
    lapw(Sr:Er,0) =  2.*w(Sr:Er,1)/(z(1)-z(0))**2 ! lapw(0:NR+1,1) !    
endif

! boundary condition are set for all processors for righ and left boundaries.
if( (para == 'R' .and. MYID == NUMPROCS-1) .or. (para == 'Z') )then
  lapw(NR+1,Sz:Ez) = 2.*w(NR,Sz:Ez)/(r(NR+1)-r(NR))**2. !lapw(NR,Sz-1:endz+2)
endif  

if((para == 'R' .and. MYID == 0) .or. (para == 'Z') )then
     lapw(0,Sz:Ez) = 2.*w(1,Sz:Ez)/(r(1)-r(0))**2. !lapw(1,Sz-1:endz+2)
endif

end subroutine CB_box_lapw
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine adapted_grid(i)
use parameters
!use tbessi
implicit none
integer, intent(in) :: i
real*8 :: dr_, dz_
! too stiff -> crah
! not well positionned -> unefiscient
! too low resolution -> crash (on the side: rapid crash ; on the middle: delayed crash)
! check the shape of the radial/axial resolution function and test it by trial/error method.



if(i==0)then

if(uni_grid)then
	r(0) = 0.
	do ir=1,NR+1
		  r(ir) = r(ir-1) +1.
	enddo	

else
	dr_ = (LRn)/(NR+2)
	r_(0) = dr_
	do ir=1,NR+1
		  r_(ir) = r_(ir-1) + dr_
	enddo
	r_ = R0n+r_


	r(0) = 0.
	do ir=1,NR+1
		  r(ir) = r(ir-1) +1.-exp(10.*(R0n-r_(ir-1)))+1.-exp(10.*(r_(ir-1)-R1n))-(1.-exp(10.*(R0n-R1n)))
	enddo
endif

r = (LRn)*r/r(NR+1)
r = R0n+r
ri = 1./r
ri2 = 1./(r**2.)

if(uni_grid)then
	z(0) = 0.
	do iz=1,NZ+1
		  z(iz) = z(iz-1) +1.
	enddo	
else
	dz_ = LZn/(NZ+2)
	z_(0) = dz_
	do iz=1,NZ+1
		  z_(iz) = z_(iz-1) + dz_
	enddo

	z(0) = 0
	do iz=1,NZ+1
		  z(iz) = z(iz-1) +1. -exp(10.*(-z_(iz-1))) +1. -exp(10.*(z_(iz-1) - LZn)) - ( 1. - exp(10.*(-LZn)) )
	enddo
endif

z = LZn*z/z(NZ+1)

if(Read_ext_data)then
call read_diag_w_lapw(2)
endif

!if(myid==0)then
!	print*, '-------------------------'
!	print*, r
!	print*, '-------------------------'
!	print*, z
!endif

! all from
!    Journal of computational physics 204 (2005) 302-318
! notation of a coeficient : 
! first (or second) letter : radial (r) or axial (z)
! second (or first) letter : m,p,n.. article notation.
! number : for laplacian (2) or for derivative (1)
! last letter : for the right (r) or left (l) scheme.

do ir = 2,NR-1
    rp1(ir)=r(ir+1) - r(ir)
    rp2(ir)=r(ir+2) - r(ir)   
    rm1(ir)=r(ir-1) - r(ir)    
    rm2(ir)=r(ir-2) - r(ir)    
enddo
    rp1(1)=r(2) - r(1)
    !rp1(0)=r(1) - r(0)
    rp1(NR)=r(NR+1) - r(NR)    
    !rp2(0)=r(2) - r(0)   
    rp2(1)=r(3) - r(1)
    
    rm1(1)=r(0) - r(1)
    rm1(NR)=r(NR-1) - r(NR)    
    !rm1(NR+1)=r(NR) - r(NR+1)    
    rm2(NR)=r(NR-2) - r(NR) 
    !rm2(NR+1)=r(NR-1) - r(NR+1) 


do iz = 2,Nz-1
    zp1(iz)=z(iz+1) - z(iz)
    zp2(iz)=z(iz+2) - z(iz)   
    zm1(iz)=z(iz-1) - z(iz)    
    zm2(iz)=z(iz-2) - z(iz)    
enddo
    zp1(1)=z(2) - z(1)
    !zp1(0)=z(1) - z(0)
    zp1(Nz)=z(Nz+1) - z(Nz)    
    !zp2(0)=z(2) - z(0)   
    zp2(1)=z(3) - z(1)
    
    zm1(1)=z(0) - z(1)
    zm1(Nz)=z(Nz-1) - z(Nz)    
    !zm1(Nz+1)=z(Nz) - z(Nz+1)    
    zm2(Nz)=z(Nz-2) - z(Nz) 
    !zm2(Nz+1)=z(Nz-1) - z(Nz+1) 

point_volume = 1.
do iz=1,NZ
  do ir=1,NR
    point_volume(ir,iz) = (zp1(iz)-zm1(iz))*(rp1(ir)-rm1(ir))/4.
  enddo
enddo

dtn = dtn_vol*minval(point_volume)

elseif(i==1)then
do ir = 1,NR
    mr1(ir) = -rp1(ir)/(rm1(ir)**2. - rp1(ir)*rm1(ir))
    nr1(ir) = -rm1(ir)/(rp1(ir)**2. - rm1(ir)*rp1(ir))
    sr1(ir) =  -(mr1(ir) +nr1(ir))
enddo   
do iz = 1,NZ
    mz1(iz) = -zp1(iz)/(zm1(iz)**2. - zp1(iz)*zm1(iz))
    nz1(iz) = -zm1(iz)/(zp1(iz)**2. - zm1(iz)*zp1(iz)) 
    sz1(iz) =  -(mz1(iz) +nz1(iz))
enddo


do ir = Sr,Er_p   
    mr2l(ir) = -2.*(rp1(ir)+rp2(ir))/(( rp1(ir)*rp2(ir) -rm1(ir)*rp1(ir) +rm1(ir)**2. -rm1(ir)*rp2(ir))*rm1(ir))
    nr2l(ir) = -2.*(rm1(ir)+rp2(ir))/((-rp1(ir)*rm1(ir) +rm1(ir)*rp2(ir) +rp1(ir)**2. -rp1(ir)*rp2(ir))*rp1(ir)) 
    pr2l(ir) =  2.*(rp1(ir)+rm1(ir))/(( rp1(ir)*rp2(ir) -rm1(ir)*rp1(ir) -rp2(ir)**2. +rm1(ir)*rp2(ir))*rp2(ir))
    sr2l(ir) = - ( mr2l(ir) +nr2l(ir) +pr2l(ir) )
enddo
if(.not.(para == 'R' .and. MYID < NUMPROCS-1))then  
    mr2r = -2.*(rp1(NR)+rm2(NR))/(( rp1(NR)*rm2(NR) -rm1(NR)*rp1(NR) +rm1(NR)**2. -rm1(NR)*rm2(NR))*rm1(NR))
    nr2r = -2.*(rm1(NR)+rm2(NR))/((-rp1(NR)*rm1(NR) +rm1(NR)*rm2(NR) +rp1(NR)**2. -rp1(NR)*rm2(NR))*rp1(NR)) 
    pr2r =  2.*(rp1(NR)+rm1(NR))/(( rp1(NR)*rm2(NR) -rm1(NR)*rp1(NR) -rm2(NR)**2. +rm1(NR)*rm2(NR))*rm2(NR))
    sr2r = - ( mr2r +nr2r +pr2r )
endif

do iz = Sz,Ez_p   
    mz2l(iz) = -2.*(zp1(iz)+zp2(iz))/(( zp1(iz)*zp2(iz) -zm1(iz)*zp1(iz) +zm1(iz)**2. -zm1(iz)*zp2(iz))*zm1(iz))
    nz2l(iz) = -2.*(zm1(iz)+zp2(iz))/((-zp1(iz)*zm1(iz) +zm1(iz)*zp2(iz) +zp1(iz)**2. -zp1(iz)*zp2(iz))*zp1(iz)) 
    pz2l(iz) =  2.*(zp1(iz)+zm1(iz))/(( zp1(iz)*zp2(iz) -zm1(iz)*zp1(iz) -zp2(iz)**2. +zm1(iz)*zp2(iz))*zp2(iz))
    sz2l(iz) = - ( mz2l(iz) +nz2l(iz) +pz2l(iz) )
enddo

if(.not.(para == 'Z' .and. MYID < NUMPROCS-1))then  
    mz2r = -2.*(zp1(NZ)+zm2(NZ))/(( zp1(NZ)*zm2(NZ) -zm1(NZ)*zp1(NZ) +zm1(NZ)**2. -zm1(NZ)*zm2(NZ))*zm1(NZ))
    nz2r = -2.*(zm1(NZ)+zm2(NZ))/((-zp1(NZ)*zm1(NZ) +zm1(NZ)*zm2(NZ) +zp1(NZ)**2. -zp1(NZ)*zm2(NZ))*zp1(NZ)) 
    pz2r =  2.*(zp1(NZ)+zm1(NZ))/(( zp1(NZ)*zm2(NZ) -zm1(NZ)*zp1(NZ) -zm2(NZ)**2. +zm1(NZ)*zm2(NZ))*zm2(NZ))
    sz2r = - ( mz2r +nz2r +pz2r )
endif

endif

end subroutine adapted_grid
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subroutine read_diag_w_lapw(i)
use parameters
use mpi_routines
implicit none
character(len=13) :: file_name
integer :: i


if(i==1)then
		write(file_name , '(A13)') 'rz_w_lapw.DAT'
		open(300, file=file_name, status = 'old', form = 'formatted')

		read(300,FMT='(I8)') NR
		read(300,FMT='(I8)') NZ
		read(300,FMT='(E15.8)') R0n
		read(300,FMT='(E15.8)') R1n
		read(300,FMT='(E15.8)') LRn
		read(300,FMT='(E15.8)') LZn
		
endif

if(i==2)then
	write(NR_string,'(I8)') NR
	write(NZ_string,'(I8)') NZ	
	
	read(300,FMT= "("//trim(adjustl(NR_string))//"(E15.8))") r(1:NR)
	read(300,FMT= "("//trim(adjustl(NZ_string))//"(E15.8))") z(1:NZ)
endif

if(i==3)then
	do iz = 1,NZ
		read(300,FMT= "("//trim(adjustl(NR_string))//"(E15.8))") fullfield(1:NR,iz)
	enddo
 w(Sr_ext:Er_ext,Sz_ext:Ez_ext) = fullfield(Sr_ext:Er_ext,Sz_ext:Ez_ext)
	do iz = 1,NZ
		read(300,FMT= "("//trim(adjustl(NR_string))//"(E15.8))") fullfield(1:NR,iz)
	enddo
	close(300)
 lapw(Sr_ext:Er_ext,Sz_ext:Ez_ext) = fullfield(Sr_ext:Er_ext,Sz_ext:Ez_ext)
endif

endsubroutine read_diag_w_lapw

end module grid_n_operators



