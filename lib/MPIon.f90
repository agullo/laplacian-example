module mpi_routines

contains

subroutine mpi_initisialisation
use parameters
implicit none
INCLUDE "mpif.h"

! Initialisation of MPI : gretting procs identities (MYID) and number of Procs (NUMPROCS).
 CALL MPI_INIT(IERR)
 CALL MPI_COMM_RANK(MPI_COMM_WORLD,MYID,IERR) 
 CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NUMPROCS,IERR)


end subroutine mpi_initisialisation
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine mpi_grid
use parameters
implicit none
INCLUDE "mpif.h"
integer :: delta, rest


if(NR > NZ) then 

    para = 'R'
    Delta = int(1.*NR/NUMPROCS)
    rest = NR - Delta* NUMPROCS
    if(MYID == 0)then
        Sr = 1
        Er = Delta
    elseif(MYID < rest+1)then
        Sr = Delta + (MYID-1)*Delta +1 +(myid-1)
        Er = Delta + (MYID)*Delta +myid
    else
        Sr = Delta + (MYID-1)*Delta +1 + rest
        Er = Delta + (MYID)*Delta + rest
    endif
    Sz = 1
    Ez = NZ
    Sz_ext = 0
    Ez_ext = NZ+1
    Ez_p = Ez-1

    Sr_ext = Sr-1
    if(MYID == NUMPROCS-1)then
        Er_ext = Er+1
        Er_p = Er - 1
    else
        Er_ext = Er+2
        Er_p = Er
    endif
else
    para = 'Z'
    Delta = int(1.*NZ/NUMPROCS)
    rest = NZ - Delta* NUMPROCS
    if(MYID == 0)then
        Sz = 1
        Ez = Delta
    elseif(MYID < rest+1)then
        Sz = Delta + (MYID-1)*Delta +1 +(myid-1)
        Ez = Delta + (MYID)*Delta +myid
    else
        Sz = Delta + (MYID-1)*Delta +1 + rest
        Ez = Delta + (MYID)*Delta + rest
    endif
    Sr = 1
    Er = NR
    Sr_ext = 0
    Er_ext = NR+1
    Er_p = Er-1

    Sz_ext = Sz-1
    if(MYID == NUMPROCS-1)then
        Ez_ext = Ez+1
        Ez_p = Ez - 1
    else
        Ez_ext = Ez+2
        Ez_p = Ez
    endif
endif

print*, para
print*, MYID, Sr, Er, Sz, Ez
print*, MYID, Sr_ext, Er_ext, Sz_ext, Ez_ext

 allocate(bufferLeft_all(1:6*NZ), bufferRight_all(1:12*NZ),SendLeft_all(1:12*NZ),SendRight_all(1:6*NZ))
 allocate(bufferBOT_all(1:6*NR), bufferTOP_all(1:12*NR),SendBOT_all(1:12*NR),SendTop_all(1:6*NR)) 

end subroutine mpi_grid
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine reducesum(a_in,a_out)
use parameters
implicit none
INCLUDE "mpif.h"
real*8, intent(in) :: a_in
real*8, intent(out) :: a_out          
      call MPI_allReduce(a_in,a_out,1,MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)     
   
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
end subroutine reducesum
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine reducemax(a_in,a_out)
use parameters
implicit none
INCLUDE "mpif.h"
real*8, intent(in) :: a_in
real*8, intent(out) :: a_out   
            
      call MPI_allReduce(a_in,a_out,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD,ierr)     
   
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
end subroutine reducemax
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine bcast_logical(a) 
use parameters
implicit none
INCLUDE "mpif.h"
logical, intent(inout) :: a
      !broad cast a logical over all processors
      call MPI_Bcast(a,1,MPI_logical,0,MPI_COMM_WORLD,ierr)
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
end subroutine bcast_logical
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine bcast_int(a) 
use parameters
implicit none
INCLUDE "mpif.h"
integer, intent(inout) :: a
      !broad cast an integer over all processors
      call MPI_Bcast(a,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
end subroutine bcast_int
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine bcast_real(a) 
use parameters
implicit none
INCLUDE "mpif.h"
real*8, intent(inout) :: a
      !broad cast a real over all processors
      call MPI_Bcast(a,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
end subroutine bcast_real
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine CB_BT_mpi_ALLfield(i,field1, field2, field3, field4, field5, field6)
use parameters
implicit none
INCLUDE "mpif.h"
integer*4, intent(in) :: i
real*8, dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext),optional, intent(inout) :: field1, field2, field3, field4, field5, field6
integer*4 :: it, i_status, Nfield
logical, save :: first_time = .true.
call MPI_barrier(MPI_COMM_WORLD,ierr)
! We send sendBOT to bufferTOP and sendTOP to bufferBOT
! sendBOT and BufferTOP contains 2 z-line because of non_uniform scheme
! sendTOP and bufferBOT contains 1 z-line.

if(present(field6))then
  Nfield = 6
elseif(present(field5))then
  Nfield = 5
elseif(present(field4))then
  Nfield = 4
elseif(present(field3))then
  Nfield = 3
elseif(present(field2))then
  Nfield = 2
elseif(present(field1))then
  Nfield = 1
endif

if(i == 0)then
       if ((MYID==0).or.(MYID==NUMPROCS-1)) then
           allocate(status_array_BT(MPI_STATUS_SIZE,1:2))
           allocate(transfert_status_BT(1:2))
       else
           allocate(status_array_BT(MPI_STATUS_SIZE,1:4))
           allocate(transfert_status_BT(1:4))
       endif
elseif(i == -1)then
           deallocate(status_array_BT)
           deallocate(transfert_status_BT)
elseif(i == 1)then


IF(MYID .NE. 0)THEN
  sendBot_all =0
  it=1
    if(present(field1))then
      do iz = Sz,Sz+1
        do ir = 1,NR
          sendBot_all(it) = field1(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field2))then
      do iz = Sz,Sz+1
        do ir = 1,NR
          sendBot_all(it) = field2(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field3))then
      do iz = Sz,Sz+1
        do ir = 1,NR
          sendBot_all(it) = field3(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field4))then
      do iz = Sz,Sz+1
        do ir = 1,NR
          sendBot_all(it) = field4(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field5))then
      do iz = Sz,Sz+1
        do ir = 1,NR
          sendBot_all(it) = field5(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field6))then
      do iz = Sz,Sz+1
        do ir = 1,NR
          sendBot_all(it) = field6(ir,iz)
          it=it+1
        enddo
      enddo
    endif
ENDIF

IF(MYID .NE. NUMPROCS-1)THEN
  sendTop_all =0
    if(present(field1))then
      sendTop_all(1:NR) = field1(1:NR,Ez)
    endif
    if(present(field2))then
      sendTop_all(1+NR:NR*2) = field2(1:NR,Ez)
    endif
    if(present(field3))then
      sendTop_all(1+NR*2:NR*3) = field3(1:NR,Ez)
    endif
    if(present(field4))then
      sendTop_all(1+NR*3:NR*4) = field4(1:NR,Ez)
    endif
    if(present(field5))then
      sendTop_all(1+NR*4:NR*5) = field5(1:NR,Ez)
    endif
    if(present(field6))then
      sendTop_all(1+NR*5:NR*6) = field6(1:NR,Ez)
    endif
ENDIF

call MPI_barrier(MPI_COMM_WORLD,ierr)

!global send recieve process to all processors in case of 1procs, boundary procs, inner procs.
   i_status = 0
IF (MYID .NE. NUMPROCS-1) THEN !bot boundary procs

   i_status = i_status +1
     call MPI_IRECV(bufferTop_all(1),2*NR*Nfield,MPI_REAL8,MYID+1,MYID+1,&
                                                         MPI_COMM_WORLD,transfert_status_BT(i_status),IERR)
   i_status = i_status +1
     call MPI_ISEND(sendTop_all(1),NR*Nfield, MPI_REAL8,MYID+1,MYID,&
                                                         MPI_COMM_WORLD,transfert_status_BT(i_status),IERR)
ENDIF
IF (MYID .NE. 0) THEN 
   i_status = i_status +1
     call MPI_IRECV(bufferBot_all(1),NR*Nfield,MPI_REAL8,MYID-1,MYID-1,&
                                                         MPI_COMM_WORLD,transfert_status_BT(i_status),IERR)
   i_status = i_status +1
     call MPI_ISEND(sendBot_all(1), 2*NR*Nfield,MPI_REAL8,MYID-1,MYID,&
                                                         MPI_COMM_WORLD,transfert_status_BT(i_status),IERR)
ENDIF

     CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)

!print*, myid, COORD2D(1), 'BT data', i_status
if(i_status .NE.0)then
 call MPI_WAITALL(i_status,transfert_status_BT(1:i_status),status_array_BT(:,1:i_status),IERR)
endif


IF(MYID .NE. NUMPROCS-1)THEN
    it=1
    if(present(field1))then
    do iz = Ez+1,Ez+2
      do ir = 1,NR
       field1(ir,iz) = BufferTOP_all(it)
       it=it+1
      enddo
    enddo
    endif
    if(present(field2))then
    do iz = Ez+1,Ez+2
      do ir = 1,NR
       field2(ir,iz) = BufferTOP_all(it)
       it=it+1
      enddo
    enddo
    endif
    if(present(field3))then
    do iz = Ez+1,Ez+2
      do ir = 1,NR
       field3(ir,iz) = BufferTOP_all(it)
       it=it+1
      enddo
    enddo
    endif
    if(present(field4))then
    do iz = Ez+1,Ez+2
      do ir = 1,NR
       field4(ir,iz) = BufferTOP_all(it)
       it=it+1
      enddo
    enddo
    endif
    if(present(field5))then
    do iz = Ez+1,Ez+2
      do ir = 1,NR
       field5(ir,iz) = BufferTOP_all(it)
       it=it+1
      enddo
    enddo
    endif
    if(present(field6))then
    do iz = Ez+1,Ez+2
      do ir = 1,NR
       field6(ir,iz) = BufferTOP_all(it)
       it=it+1
      enddo
    enddo
    endif
ENDIF

IF(MYID .NE. 0)THEN
    if(present(field1))then
      field1(1:NR,Sz-1) = BufferBOT_all(1:NR) 
    endif
    if(present(field2))then
      field2(1:NR,Sz-1) = BufferBOT_all(NR+1:NR*2) 
    endif
    if(present(field3))then
      field3(1:NR,Sz-1) = BufferBOT_all(NR*2+1:NR*3) 
    endif
    if(present(field4))then
      field4(1:NR,Sz-1) = BufferBOT_all(NR*3+1:NR*4) 
    endif
    if(present(field5))then
      field5(1:NR,Sz-1) = BufferBOT_all(NR*4+1:NR*5) 
    endif
    if(present(field6))then
      field6(1:NR,Sz-1) = BufferBOT_all(NR*5+1:NR*6) 
    endif
ENDIF

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
endif

end subroutine CB_BT_mpi_ALLfield
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine CB_RL_mpi_ALLfield(i,field1, field2, field3, field4, field5, field6)
use parameters
implicit none
INCLUDE "mpif.h"
integer*4, intent(in) :: i
real*8, dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext),optional, intent(inout) :: field1, field2, field3, field4, field5, field6
integer*4 :: it, i_status, Nfield
logical, save :: first_time = .true.
call MPI_barrier(MPI_COMM_WORLD,ierr)
! We send sendLeft to bufferTOP and sendTOP to bufferLeft
! sendLeft and BufferTOP contains 2 z-line because of non_uniform scheme
! sendTOP and bufferLeft contains 1 z-line.

if(present(field6))then
  Nfield = 6
elseif(present(field5))then
  Nfield = 5
elseif(present(field4))then
  Nfield = 4
elseif(present(field3))then
  Nfield = 3
elseif(present(field2))then
  Nfield = 2
elseif(present(field1))then
  Nfield = 1
endif

if(i == 0)then
       if ((MYID==0).or.(MYID==NUMPROCS-1)) then
           allocate(status_array_RL(MPI_STATUS_SIZE,1:2))
           allocate(transfert_status_RL(1:2))
       else
           allocate(status_array_RL(MPI_STATUS_SIZE,1:4))
           allocate(transfert_status_RL(1:4))
       endif
elseif(i == -1)then
           deallocate(status_array_RL)
           deallocate(transfert_status_RL)
elseif(i == 1)then

IF(MYID.NE. 0)THEN
  sendLeft_all =0
  it=1
    if(present(field1))then
    do iz = 1,NZ
      do ir = Sr,Sr+1
          sendLeft_all(it) = field1(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field2))then
    do iz = 1,NZ
      do ir = Sr,Sr+1
          sendLeft_all(it) = field2(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field3))then
    do iz = 1,NZ
      do ir = Sr,Sr+1
          sendLeft_all(it) = field3(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field4))then
    do iz = 1,NZ
      do ir = Sr,Sr+1
          sendLeft_all(it) = field4(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field5))then
    do iz = 1,NZ
      do ir = Sr,Sr+1
          sendLeft_all(it) = field5(ir,iz)
          it=it+1
        enddo
      enddo
    endif
    if(present(field6))then
    do iz = 1,NZ
      do ir = Sr,Sr+1
          sendLeft_all(it) = field6(ir,iz)
          it=it+1
        enddo
      enddo
    endif
ENDIF


IF(MYID .NE. NUMPROCS-1)THEN
  sendRight_all =0
    if(present(field1))then
      sendRight_all(1:NZ) = field1(Er,1:NZ)
    endif
    if(present(field2))then
      sendRight_all(1+NZ:NZ*2) = field2(Er,1:NZ)
    endif
    if(present(field3))then
      sendRight_all(1+NZ*2:NZ*3) = field3(Er,1:NZ)
    endif
    if(present(field4))then
      sendRight_all(1+NZ*3:NZ*4) = field4(Er,1:NZ)
    endif
    if(present(field5))then
      sendRight_all(1+NZ*4:NZ*5) = field5(Er,1:NZ)
    endif
    if(present(field6))then
      sendRight_all(1+NZ*5:NZ*6) = field6(Er,1:NZ)
    endif
ENDIF

call MPI_barrier(MPI_COMM_WORLD,ierr)

!global send recieve process to all processors in case of 1procs, boundary procs, inner procs.
   i_status = 0
IF (MYID .NE. NUMPROCS-1) THEN !Left boundary procs
   i_status = i_status +1
     call MPI_IRECV(bufferRight_all(1),2*NZ*Nfield,MPI_REAL8,MYID+1,MYID+1,&
                                                                        MPI_COMM_WORLD,transfert_status_RL(i_status),IERR)
   i_status = i_status +1
     call MPI_ISEND(sendRight_all(1), NZ*Nfield, MPI_REAL8,MYID+1,MYID,&
                                                                        MPI_COMM_WORLD,transfert_status_RL(i_status),IERR)
ENDIF
IF (MYID.NE. 0) THEN
  ! i_status = 0
   i_status = i_status +1
     call MPI_IRECV(bufferLeft_all(1),NZ*Nfield,MPI_REAL8,MYID-1,MYID-1,&
                                                                        MPI_COMM_WORLD,transfert_status_RL(i_status),IERR)
   i_status = i_status +1
     call MPI_ISEND(sendLeft_all(1), 2*NZ*Nfield,MPI_REAL8,MYID-1,MYID  ,&
                                                                        MPI_COMM_WORLD,transfert_status_RL(i_status),IERR)
ENDIF

  !   CALL MPI_BARRIER(COMM2D,IERR)
!print*, myid2D, 'comm done'
!print*, myid, COORD2D(1), 'BT data', i_status
if(i_status .NE.0)then
 call MPI_WAITALL(i_status,transfert_status_RL(1:i_status),status_array_RL(:,1:i_status),IERR)
endif

IF(MYID.NE. NUMPROCS-1)THEN
  it=1
    if(present(field1))then
    do iz = 1,NZ
      do ir = Er+1,Er+2 
        field1(ir,iz) = BufferRight_all(it)
        it=it+1
      enddo
    enddo
    endif
    if(present(field2))then
    do iz = 1,NZ
      do ir = Er+1,Er+2 
        field2(ir,iz) = BufferRight_all(it)
        it=it+1
      enddo
    enddo
    endif
    if(present(field3))then
    do iz = 1,NZ
      do ir = Er+1,Er+2 
        field3(ir,iz) = BufferRight_all(it)
        it=it+1
      enddo
    enddo
    endif
    if(present(field4))then
    do iz = 1,NZ
      do ir = Er+1,Er+2 
        field4(ir,iz) = BufferRight_all(it)
        it=it+1
      enddo
    enddo
    endif
    if(present(field5))then
    do iz = 1,NZ
      do ir = Er+1,Er+2 
        field5(ir,iz) = BufferRight_all(it)
        it=it+1
      enddo
    enddo
    endif
    if(present(field6))then
    do iz = 1,NZ
      do ir = Er+1,Er+2 
        field6(ir,iz) = BufferRight_all(it)
        it=it+1
      enddo
    enddo
    endif
ENDIF

IF(MYID.NE. 0)THEN
    if(present(field1))then
      field1(Sr-1,1:NZ) = BufferLeft_all(1:NZ) 
    endif
    if(present(field2))then
      field2(Sr-1,1:NZ) = BufferLeft_all(NZ+1:NZ*2) 
    endif
    if(present(field3))then
      field3(Sr-1,1:NZ) = BufferLeft_all(NZ*2+1:NZ*3) 
    endif
    if(present(field4))then
      field4(Sr-1,1:NZ) = BufferLeft_all(NZ*3+1:NZ*4) 
    endif
    if(present(field5))then
      field5(Sr-1,1:NZ) = BufferLeft_all(NZ*4+1:NZ*5) 
    endif
    if(present(field6))then
      field6(Sr-1,1:NZ) = BufferLeft_all(NZ*5+1:NZ*6) 
    endif
ENDIF

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
endif
end subroutine CB_RL_mpi_ALLfield
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine gather_field(field)
use parameters
implicit none
INCLUDE "mpif.h"
real*8, dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext), intent(in) :: field
real*8, dimension(1:NR*NZ) :: fullline
real*8, dimension(1:(Ez-Sz+1)*(Er-Sr+1)) :: line
integer*4 :: il, ip
logical, save :: first_call=.True.
integer*4 :: Delta
integer*4, dimension(:),allocatable,save :: Deltas, Disps,Ezs,Szs,Ers,Srs


call MPI_BARRIER(MPI_COMM_WORLD,IERR)

if(first_call)then
allocate(Deltas(1:NUMPROCS), Disps(1:NUMPROCS),Ezs(1:NUMPROCS),Szs(1:NUMPROCS),Ers(1:NUMPROCS),Srs(1:NUMPROCS))
  first_call=.false.
endif

  Delta = (Ez-Sz+1)*(Er-Sr+1)
  CALL MPI_allGather( Delta, 1, MPI_INTEGER, Deltas,1,MPI_INTEGER,MPI_COMM_WORLD,IERR)
  CALL MPI_allGather( Sr, 1, MPI_INTEGER, Srs,1,MPI_INTEGER,MPI_COMM_WORLD,IERR)
  CALL MPI_allGather( Er, 1, MPI_INTEGER, Ers,1,MPI_INTEGER,MPI_COMM_WORLD,IERR)
  CALL MPI_allGather( Sz, 1, MPI_INTEGER, Szs,1,MPI_INTEGER,MPI_COMM_WORLD,IERR)
  CALL MPI_allGather( Ez, 1, MPI_INTEGER, Ezs,1,MPI_INTEGER,MPI_COMM_WORLD,IERR)
    Disps(1) = 0
    do ip = 2,NUMPROCS
      Disps(ip) =Disps(ip-1) + Deltas(ip-1)
    enddo

!print*, myid,' | ' , deltas
!print*, myid,' | ' , disps

il=1
do iz=Sz,Ez
    do ir=Sr,Er
    line(il) = field(ir,iz)
    il=il+1
    enddo
enddo

CALL MPI_GATHERV( line(1), Delta, MPI_REAL8, fullline(1), Deltas, Disps, MPI_REAL8,0,MPI_COMM_WORLD,IERR)

if(MYID==0)then
  il=1
do ip=1,NUMPROCS
  do iz=Szs(ip),Ezs(ip)
    do ir=Srs(ip),Ers(ip)
    fullfield(ir,iz) = fullline(il)
    il=il+1
    enddo
  enddo
enddo
endif
call MPI_BARRIER(MPI_COMM_WORLD,IERR)

end subroutine gather_field
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine wait_all_here
use parameters
implicit none
INCLUDE "mpif.h"

 call MPI_BARRIER(MPI_COMM_WORLD,IERR)
 
end subroutine wait_all_here
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine MPI_final
use parameters
implicit none
INCLUDE "mpif.h"
 call MPI_BARRIER(MPI_COMM_WORLD,IERR)
 call MPI_FINALIZE(IERR)
 deallocate(bufferLeft_all, bufferRight_all, SendLeft_all, SendRight_all)
 deallocate(bufferBOT_all,  bufferTOP_all,   SendBOT_all,  SendTop_all) 
end subroutine MPI_final

end module MPI_routines
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!subroutine mpi_passing_bound_twinkle
!use parameters
!implicit none
!INCLUDE "mpif.h"

! !print*, MYID,iteration, 'I finished set twinkle comm, I start BT comm, I got to send',Nb_req_by_top, Nb_req_by_bot
!  if(NPZ /= 1) then
!      call CB_BT_mpi_field_twinkle(U)
!      call CB_BT_mpi_field_twinkle(B)
!      call CB_BT_mpi_field_twinkle(w)
!      call CB_BT_mpi_field_twinkle(psi)
!      call CB_BT_mpi_field_twinkle(lapw)     
!      call CB_BT_mpi_field_twinkle(lappsi)
!  endif
 !print*, MYID,iteration, 'I finished BT comm, I start RL comm'

!  if(NPR /= 1)then
!     call CB_RL_mpi_field_twinkle(U)
!     call CB_RL_mpi_field_twinkle(B)
!      call CB_RL_mpi_field_twinkle(w)
!      call CB_RL_mpi_field_twinkle(psi)
!      call CB_RL_mpi_field_twinkle(lapw)  
!      call CB_RL_mpi_field_twinkle(lappsi)
!  endif


 !print*, MYID,iteration, 'I finished RL comm'
!end subroutine mpi_passing_bound_twinkle
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!subroutine CB_BT_mpi_field_twinkle(A_field)
!use parameters
!implicit none
!INCLUDE "mpif.h"
!real*8, dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext), intent(inout) :: A_field
!integer*4 :: it, i_status

!call MPI_barrier(COMM2D,ierr)
!! We send sendBOT to bufferTOP and sendTOP to bufferBOT
!! sendBOT and BufferTOP contains 2 z-line because of non_uniform scheme
!! sendTOP and bufferBOT contains 1 z-line.

!if(COORD2D(1)==0)then
!    sendTop =0
!    bufferTop =0
!  if(Nb_req_by_top .NE. 0)then
!    sendTop(1:Nb_req_by_top) = A_field(coord_bufferTop(1:Nb_req_by_top),Ez)
!    !sendTop(1:Nb_req_by_top) = -100*Ez -coord_bufferTop(1:Nb_req_by_top)
!  endif
!elseif(COORD2D(1)==NPZ-1)then
!        sendBot =0
!        bufferBot =0
!  if(Nb_req_by_bot .NE. 0)then
!    it=1
!    do iz = Sz,Sz+1
!      do ir = 1,Nb_req_by_bot
!        sendBot(it) = A_field(coord_bufferBot(ir),iz)
!        !sendBot(it) = -100*iz -coord_bufferBot(ir)
!        it=it+1
!      enddo
!    enddo
!  endif
!else
!    bufferTop =0
!    bufferBot =0
!    sendBot =0
!    sendTop =0
!  if(Nb_req_by_top .NE. 0)then
!    sendTop(1:Nb_req_by_top) = A_field(coord_bufferTop(1:Nb_req_by_top),Ez)
!    !sendTop(1:Nb_req_by_top) = -100*Ez -coord_bufferTop(1:Nb_req_by_top)
!  endif
!  if(Nb_req_by_bot .NE. 0)then
!    it=1
!    do iz = Sz,Sz+1
!      do ir = 1,Nb_req_by_bot
!        sendBot(it) = A_field(coord_bufferBot(ir),iz)
!        !sendBot(it) = -100*iz -coord_bufferBot(ir)
!        it=it+1
!      enddo
!    enddo
!  endif
!endif


!call MPI_barrier(COMM2D,ierr)

!!global send recieve process to all processors in case of 1procs, boundary procs, inner procs.
!IF (COORD2D(1)==0) THEN !bot boundary procs
!   i_status = 0
!   if(EtEz .NE. 0)then
!   i_status = i_status +1
!     call MPI_IRECV(bufferTop(1),2*EtEz,MPI_REAL8,MYTOP_neighboor,MYTOP_neighboor,COMM2D,transfert_status_BT(i_status),IERR)
!   endif
!   if(Nb_req_by_top .NE. 0)then
!   i_status = i_status +1
!     call MPI_ISEND(sendTop(1), Nb_req_by_top, MPI_REAL8,MYTOP_neighboor,MYID2D,COMM2D,transfert_status_BT(i_status),IERR)
!   endif
!ELSEIF (COORD2D(1)==NPZ-1) THEN !top boundary procs
!   i_status = 0
!   if(EtSz .NE. 0)then
!   i_status = i_status +1
!     call MPI_IRECV(bufferBot(1),EtSz,MPI_REAL8,MYBOT_neighboor,MYBOT_neighboor,COMM2D,transfert_status_BT(i_status),IERR)
!   endif
!   if(Nb_req_by_bot .NE. 0)then
!   i_status = i_status +1
!     call MPI_ISEND(sendBot(1), 2*Nb_req_by_bot,MPI_REAL8,MYBOT_neighboor,MYID2D  ,COMM2D,transfert_status_BT(i_status),IERR)
!   endif
!ELSE
!   i_status = 0
!   if(EtEz .NE. 0)then
!   i_status = i_status +1
!     call MPI_IRECV(bufferTop(1),2*EtEz,MPI_REAL8,MYTOP_neighboor,MYTOP_neighboor,COMM2D,transfert_status_BT(i_status),IERR)
!   endif
!   if(EtSz .NE. 0)then
!   i_status = i_status +1
!     call MPI_IRECV(bufferBot(1),EtSz,MPI_REAL8,MYBOT_neighboor,MYBOT_neighboor,COMM2D,transfert_status_BT(i_status),IERR)
!   endif
!   if(Nb_req_by_bot .NE. 0)then
!   i_status = i_status +1
!     call MPI_ISEND(sendBot(1), 2*Nb_req_by_bot, MPI_REAL8,MYBOT_neighboor,MYID2D  ,COMM2D,transfert_status_BT(i_status),IERR)
!    endif
!   if(Nb_req_by_top .NE. 0)then
!   i_status = i_status +1
!     call MPI_ISEND(sendTop(1), Nb_req_by_top, MPI_REAL8,MYTOP_neighboor,MYID2D  ,COMM2D,transfert_status_BT(i_status),IERR)
!   endif
!ENDIF

!     CALL MPI_BARRIER(COMM2D,IERR)

!print*, myid, COORD2D(1), 'BT data', i_status
!if(i_status .NE.0)then
! call MPI_WAITALL(i_status,transfert_status_BT(1:i_status),status_array_BT(:,1:i_status),IERR)
!endif


!if(COORD2D(1)==NPZ-1)then
!  if(EtSz .NE. 0)then
!    A_field(TwinkleSz(1:EtSz),Sz-1) = BufferBOT(1:EtSz) 
!  endif
!elseif(COORD2D(1)==0)then
!  if(EtEz .NE. 0)then
!    it=1
!    do iz = Ez+1,Ez+2
!      do ir = 1,EtEz
!       A_field(TwinkleEz(ir),iz) = BufferTOP(it)
!       it=it+1
!      enddo
!    enddo
!  endif
!else
! if( EtSz .NE. 0)then
!    A_field(TwinkleSz(1:EtSz),Sz-1) = BufferBOT(1:EtSz) 
! endif
! if(EtEz .NE. 0)then
!  it=1
!  do iz = Ez+1,Ez+2
!    do ir = 1,EtEz
!     A_field(TwinkleEz(ir),iz) = BufferTOP(it)
!     it=it+1
!    enddo
!  enddo
! endif
!endif

!      CALL MPI_BARRIER(COMM2D,IERR)

!end subroutine CB_BT_mpi_field_twinkle
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!subroutine CB_RL_mpi_field_twinkle(A_field)
!use parameters
!implicit none
!INCLUDE "mpif.h"
!real*8, dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext), intent(inout) :: A_field
!integer*4 :: it, i_status

!! We send sendLeft to bufferRight and sendRight to bufferLeft
!! sendLeft and BufferRight contains 2 z-line because of non_uniform scheme
!! sendRight and bufferLeft contains 1 z-line.


!if(COORD2D(0)==0)then 
!  if(Nb_req_by_Right.NE. 0 )then
!    sendRight(1:Nb_req_by_Right) = A_field(Er,coord_bufferRight(1:Nb_req_by_Right))
!  endif
!elseif(COORD2D(0)==NPR-1)then
!  if(Nb_req_by_Left.NE. 0)then
!    it=1
!    do iz = 1,Nb_req_by_Left
!      do ir = Sr,Sr+1
!        sendLeft(it) = A_field(ir,coord_bufferLeft(iz))
!        it=it+1
!      enddo
!    enddo
!  endif
!else
! if(Nb_req_by_Right.NE. 0 )then
!    sendRight(1:Nb_req_by_Right) = A_field(Er,coord_bufferRight(1:Nb_req_by_Right))
! endif
! if(Nb_req_by_Left.NE. 0 )then
!  it=1
!  do iz = 1,Nb_req_by_Left
!    do ir = Sr,Sr+1
!      sendLeft(it) = A_field(ir,coord_bufferLeft(iz))
!      it=it+1
!    enddo
!  enddo
! endif
!endif

!!call MPI_barrier(MPI_COMM_WORLD,ierr)

!!global send recieve process to all processors in case of 1procs, boundary procs, inner procs.
!IF (COORD2D(0)==0) THEN !Left boundary procs
!   i_status = 0
!   if(EtEr.NE. 0)then
!     i_status = i_status +1
!     call MPI_IRECV(bufferRight(1),2*EtEr,MPI_REAL8,MYRight_neighboor,MYRight_neighboor,COMM2D,transfert_status_RL(i_status),IERR)
!   endif
!   if(Nb_req_by_Right.NE. 0)then
!     i_status = i_status +1
!     call MPI_ISEND(sendRight(1), Nb_req_by_Right, MPI_REAL8,MYRight_neighboor,MYID2D,COMM2D,transfert_status_RL(i_status),IERR)
!   endif
!ELSEIF (COORD2D(0)==NPR-1) THEN !Right boundary procs
!   i_status = 0
!   if(EtSr.NE.0)then
!     i_status = i_status +1
!     call MPI_IRECV(bufferLeft(1),EtSr,MPI_REAL8,MYLeft_neighboor,MYLeft_neighboor,COMM2D,transfert_status_RL(i_status),IERR)
!   endif
!   if(Nb_req_by_Left.NE.0)then
!     i_status = i_status +1
!     call MPI_ISEND(sendLeft(1), 2*Nb_req_by_Left,MPI_REAL8,MYLeft_neighboor,MYID2D  ,COMM2D,transfert_status_RL(i_status),IERR)
!   endif
!ELSE
!   i_status = 0
!   if(EtEr.NE. 0)then
!     i_status = i_status +1
!     call MPI_IRECV(bufferRight(1),2*EtEr,MPI_REAL8,MYRight_neighboor,MYRight_neighboor,COMM2D,transfert_status_RL(i_status),IERR)
!   endif
!   if(EtSr.NE. 0)then
!     i_status = i_status +1
!     call MPI_IRECV(bufferLeft(1),EtSr,MPI_REAL8,MYLeft_neighboor,MYLeft_neighboor,COMM2D,transfert_status_RL(i_status),IERR)
!   endif
!   if(Nb_req_by_Left.NE. 0)then
!     i_status = i_status +1
!     call MPI_ISEND(sendLeft(1), 2*Nb_req_by_Left, MPI_REAL8,MYLeft_neighboor,MYID2D  ,COMM2D,transfert_status_RL(i_status),IERR)
!   endif
!   if(Nb_req_by_Right.NE. 0)then
!     i_status = i_status +1
!     call MPI_ISEND(sendRight(1), Nb_req_by_Right, MPI_REAL8,MYRight_neighboor,MYID2D  ,COMM2D,transfert_status_RL(i_status),IERR)
!   endif

!ENDIF
!!print*, MYID, 'I send/Receive the data'


!if(i_status .NE.0)then
!!print*, MYID2D,'||', lbound(transfert_status_RL),i_status,ubound(transfert_status_RL)
!! call MPI_WAITALL(i_status,transfert_status_RL(1:i_status),status_array_RL(:,1:i_status),IERR)
! call MPI_WAITALL(i_status,transfert_status_RL(1:i_status),MPI_STATUSES_IGNORE,IERR)
!endif


!if(COORD2D(0)==0)then
!  if(EtEr.NE. 0)then
!    it = 1
!    do iz = 1,EtEr
!      do ir = Er+1,Er+2 
!        A_field(ir,TwinkleEr(iz)) = BufferRight(it)
!        it=it+1
!      enddo
!    enddo
!  endif
!elseif(COORD2D(0)==NPR-1)then
!  if(EtSr .NE. 0)then
!    A_field(Sr-1,TwinkleSr(1:EtSr)) = BufferLeft(1:EtSr) 
!  endif
!else
!  if(EtEr.NE. 0) then
!    it = 1
!    do iz = 1,EtEr
!      do ir = Er+1,Er+2 
!        A_field(ir,TwinkleEr(iz)) = BufferRight(it)
!        it=it+1
!      enddo
!    enddo
!  endif
!  if(EtSr .NE. 0)then
!    A_field(Sr-1,TwinkleSr(1:EtSr)) = BufferLeft(1:EtSr) 
!  endif
!endif

 
!!print*, MYID2D, 'RL comm over'
!      CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
  
!end subroutine CB_RL_mpi_field_twinkle
!!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
