;; The ‘guix.scm’ file for Guile, for use by ‘guix shell’.

(define-module (laplacian-example-package)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages ssh)
  #:use-module (guix build-system python)
  #:use-module (srfi srfi-1)
   )

(define-public laplacian-example
  (let ((vcs-file? (or (git-predicate
                        (string-append (current-source-directory)
                                       "/../.."))
                       (const #t))))

  (package
    (name "laplacian-example")
    (version "1.0.0-git")
    (home-page "https://gitlab.inria.fr/agullo/laplacian-example")
    (source (local-file "../.." "laplacian-example-checkout"
                        #:recursive? #t
                        #:select? vcs-file?))
    ;; (source (origin
    ;;           (method git-fetch)
    ;;           (uri (git-reference
    ;;                 (url home-page)
    ;;                 (commit "8417d5da6eec9a93c7fb884be7ae5d519926c236")
    ;;                 ;; We need the submodule in 'cmake_modules/morse'.
    ;;                 (recursive? #t)))
    ;;           (file-name (string-append name "-" version "-checkout"))
    ;;           (sha256
    ;;            (base32
    ;;             "1cs1fifpqqva37n1qqq05h6vmliy25xhjg8dr9zlv60zpibac5r1"))))
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DLAPLACIAN_USE_MPI=OFF"
                           "-DLAPLACIAN_BUILD_EXAMPLES=ON"
                           )
       #:tests? #f))
       ;; #:phases (modify-phases %standard-phases
       ;;            ;; Without this variable, pkg-config removes paths in already in CFLAGS
       ;;            ;; However, gfortran does not check CPATH to find fortran modules
       ;;            ;; and and the module fabulous_mod cannot be found
       ;;            (add-before 'configure 'fix-pkg-config-env
       ;;              (lambda _ (setenv "PKG_CONFIG_ALLOW_SYSTEM_CFLAGS" "1")))
       ;;            (add-before 'configure 'set-fortran-flags
       ;;              (lambda _
       ;;                (define supported-flag?
       ;;                  ;; Is '-fallow-argument-mismatch' supported?  It is
       ;;                  ;; supported by GCC 10 but not by GCC 7.5.
       ;;                  (zero? (system* "gfortran" "-c" "-o" "/tmp/t.o"
       ;;                                  "/dev/null" "-fallow-argument-mismatch")))

       ;;                (when supported-flag?
       ;;                  (substitute* "CMakeLists.txt"
       ;;                    ;; Pass '-fallow-argument-mismatch', which is
       ;;                    ;; required when building with GCC 10+.
       ;;                    (("-ffree-line-length-0")
       ;;                     "-ffree-line-length-0 -fallow-argument-mismatch")))))
       ;;            ;; Allow tests with more MPI processes than available CPU cores,
       ;;            ;; which is not allowed by default by OpenMPI
       ;;            (add-before 'check 'prepare-test-environment
       ;;              (lambda _
       ;;                (setenv "OMPI_MCA_rmaps_base_oversubscribe" "1"))))))
    (inputs (list  openmpi
                  openssh
                  openblas))
    (native-inputs (list gfortran pkg-config))
    (synopsis "Example of solving a Laplacian problem in Fortran")
    (description
     "LAPLACIAN is an example of a Laplacian problem. The code is written in Fortran 90.")
    (license license:cecill-c))))

;; Return the package object defined above at the end of the module.
laplacian-example
