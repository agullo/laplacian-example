
format long

path = './';
file = 'testIlap.txt';

fid = fopen(strcat(path,'test_gri.txt'),'r');
r = str2num(fgetl(fid));
z = str2num(fgetl(fid));
fclose(fid);

NR = length(r);
NZ = length(z);
fid = fopen(strcat(path,file),'r');
field = zeros(NZ,NR);
for iz = 1:NZ
    field(iz,:) = str2num(fgetl(fid));
end
fclose(fid)
figure(5)
pcolor(r,z,field)
